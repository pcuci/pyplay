import sys

sys.path.append("../")

import play
import unittest


class Test_TestIncrementDecrement(unittest.TestCase):
    def test_increment(self):
        print("hello test debuggin")
        self.assertEqual(play.increment(3), 4)

    def test_decrement(self):
        self.assertEqual(play.decrement(3), 2)


if __name__ == "__main__":
    unittest.main()
